import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './rootReducer';


const store = createStore(
  compose(
    applyMiddleware(thunk),
  ),
);


export { store };
