import { handleActions, createActions } from 'redux-actions';
import { createSelector } from 'reselect';
// import api from 'lib/api';

const initialState = {
  loading: null,
};

const actions = createActions('TEST_ACTION');

const effects = {
};

const reducer = handleActions(
  {
    // [actions.fetchPricesRequest]: state => ({
    //   ...state,
    //   loading: true,
    // }),
  },
  initialState,
);

const getState = (state) => state.main;

const cs = (cb) => createSelector(
  [getState],
  cb,
);

const selectors = {
  getPrices: cs((s) => s.prices),
  getQuestions: cs((s) => s.questions),
  getLoading: cs((s) => s.loading),
  getErrors: cs((s) => s.error),
  getFeedback: cs((s) => s.feedback),
};

export {
  initialState as state, actions, effects, reducer, selectors,
};
