import React from 'react';
import { Provider } from 'react-redux';
import { Router } from 'react-router-dom';
import history from './lib/history';
import { store } from './redux/store';
import Routes from './views/RootRoutes';
import './styles/main.scss';

const App = () => (
  <Provider store={store}>
    <Router history={history}>
      <>
        <Routes />
      </>
    </Router>
  </Provider>
);

export default App;
