import React, { Suspense } from 'react';
import { connect } from 'react-redux';
import { Switch, Route } from 'react-router-dom';
import Header from 'components/Header';

import Home from './Home';

const RootRoutes = () => (
    <div>
      <Header />
      <Suspense fallback={<div>Загрузка...</div>}>
        <Switch>
          <Route exact path="/" component={Home} />
        </Switch>
      </Suspense>
    </div>
  );

export default connect(
  null,
  null,
)(RootRoutes);
