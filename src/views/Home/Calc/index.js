import React, { useState } from 'react';
import Range from './Range';
import css from './styles.module.scss';

const Calc = () => {
  const [cost, setCost] = useState([240000]);
  const [prepayment, setPrepayment] = useState([10]);
  const [last, setLast] = useState([0]);
  const [contract, setContract] = useState([12]);
  // const [lisings, setLisings] = useState([{
  //   cost: {
  //     min: 240000,
  //     max: 50000000,
  //     step: 10000,
  //     value: 240000,
  //   },
  //   prepayment: {
  //     min: 10,
  //     max: 49,
  //     step: 1,
  //   },
  //   last: {
  //     min: 0,
  //     max: 40,
  //     step: 1,
  //   },
  //   contract: {
  //     min: 12,
  //     max: 48,
  //     step: 1,
  //   },
  // }]);

  // const onChange = (value,name) => {
  //   setLisings([...lisings])
  // };

  return (
    <div className={css.calc}>
      <div className="container">
        <h2>Калькулятор</h2>

        <div className={css.items}>
          <div className={css.row}>
            <div className={css.col}>
              <div className={css.mainCalc}>
                <Range
                  value={cost}
                  onChange={setCost}
                  min={240000}
                  max={50000000}
                  step={10000}
                  title="Стоимость имущества"
                  symbol="₽"
                />
                <Range
                  value={prepayment}
                  onChange={setPrepayment}
                  min={10}
                  max={49}
                  step={1}
                  title="Размер аванса"
                  symbol="%"
                />
                <Range
                  value={contract}
                  onChange={setContract}
                  min={12}
                  max={48}
                  step={1}
                  title="Срок договора"
                  symbol="месяцев"
                />
              </div>
            </div>

            <div className={css.col}>
              <div className={css.result}>
                <h3>Результат расчета</h3>
                <strong>
                  <span>22 417 ₽</span>
                  <span> Ежемесячный платеж от</span>
                </strong>

                <div>
                  <span>297 001 ₽</span>
                  <span>Сумма договора</span>
                </div>

                <div>
                  <span>10.00%</span>
                  <span>Годовое удорожание</span>
                </div>

                <button type="button">Подать заявку</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Calc;
