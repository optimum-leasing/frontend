import React from 'react';
import { Range, getTrackBackground } from 'react-range';
import css from './styles.module.scss';

const RangeInput = ({
 value, step, min, max, onChange, title, symbol 
}) => (
  <div className={css.range}>
    <div className={css.items}>
      <strong>{title}</strong>
      <span>
        {value[0]} 
{' '}
{symbol}
      </span>
    </div>
    <Range
      values={value}
      step={step}
      min={min}
      max={max}
      onChange={onChange}
      renderTrack={({ props, children }) => (
        <div
          onMouseDown={props.onMouseDown}
          onTouchStart={props.onTouchStart}
          style={{
            ...props.style,
            height: '36px',
            display: 'flex',
            width: '100%',
          }}
        >
          <div
            ref={props.ref}
            style={{
              height: '5px',
              width: '100%',
              borderRadius: '4px',
              background: getTrackBackground({
                values: value,
                colors: ['#ac6bcd', '#ccc'],
                min,
                max,
              }),
              alignSelf: 'center',
            }}
          >
            {children}
          </div>
        </div>
      )}
      renderThumb={({ props, isDragged }) => (
        <div
          {...props}
          style={{
            ...props.style,
            height: '22px',
            width: '22px',
            borderRadius: '2px',
            backgroundColor: '#FFF',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            boxShadow: '0px 2px 6px #AAA',
          }}
        >
          <div
            style={{
              height: '10px',
              width: '4px',
              backgroundColor: isDragged ? '#ac6bcd' : '#CCC',
            }}
          />
        </div>
      )}
    />
  </div>
);

export default RangeInput;
