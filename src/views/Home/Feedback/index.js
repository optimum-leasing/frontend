import React from 'react';
import css from './styles.module.scss';

const Feedback = () => (
  <div className={css.feedback}>
    <div className="container">
      <h2>Хотите воспользоваться лизингом, но не знаете, с чего начать?</h2>
      <p>
        Напишите нам или закажите звонок — мы поможем. Выслушаем ваши задачи для
        бизнеса, и на их основе предложим варианты развития.
      </p>
      <button type="buttton">Оставить заявку</button>
    </div>
  </div>
);

export default Feedback;
