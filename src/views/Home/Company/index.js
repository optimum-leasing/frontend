import React from 'react';
import css from './styles.module.scss';

const Company = () => (
  <div className={css.company}>
    <div className="container">
      <div className={css.items}>
        <div className={css.content}>
          <h2>О Компании</h2>
          <p>
            Lorem ipsum dolor sit, amet consectetur adipisicing elit. Architecto
            maxime sint minima optio maiores ex excepturi. Iusto consequatur,
            non suscipit dicta harum molestiae doloribus necessitatibus quo
            molestias atque pariatur incidunt?
          </p>
          <p>
            Lorem ipsum dolor sit, amet consectetur adipisicing elit. Tempore
            neque voluptatum quisquam reiciendis rem molestiae, ullam iusto vero
            eum quaerat doloribus repellendus debitis molestias voluptas animi
            at perferendis similique minus.
          </p>

          <button type="button">
            <span>Подробнее</span>
          </button>
        </div>

        <div
          className={css.bg}
          style={{
            backgroundImage:
              'url(http://www.akabi.eu/Content/images/black-and-white-city-man-people.jpg)',
          }}
        />
      </div>
    </div>
  </div>
);

export default Company;
