import React from 'react';
import { MdAccountBalance } from 'react-icons/md';
import css from './styles.module.scss';

const Services = () => (
  <div className={css.services}>
    <div className="container">
      <h2>Почему мы?</h2>

      <div className={css.items}>
        <div className={css.item}>
          <div>
            <MdAccountBalance />
            <h4>Более 2 000 компаний</h4>
            <p>
              Lorem ipsum dolor sit, amet consectetur adipisicing elit. Iste
              blanditiis a vitae libero ex! Fugit dolores corporis quaerat
              debitis quis reprehenderit
            </p>
          </div>
        </div>

        <div className={css.item}>
          <div>
            <MdAccountBalance />
            <h4>Гарантия результата</h4>
            <p>
              Lorem ipsum dolor sit amet consectetur, adipisicing elit.
              Laudantium omnis sunt deserunt similique facere ut enim magni
              voluptates magnam ea officiis sint ab.
            </p>
          </div>
        </div>

        <div className={css.item}>
          <div>
            <MdAccountBalance />
            <h4>Мобильность</h4>
            <p>
              Lorem ipsum dolor sit amet consectetur, adipisicing elit.
              Necessitatibus.
            </p>
          </div>
        </div>

        <div className={css.feedback}>
          <div>
            <h4>
              Наша компания очень хорошая и целеустремленная (мотивирущий текст)
            </h4>
            <button type="button">Наш каталог</button>
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default Services;
