import React from 'react';
import bg from 'assets/bg-1.jpg';
import css from './styles.module.scss';

const Listings = () => (
  <section className={css.listing}>
    <div className="container">
      <h2>Основные лизинговые продукты</h2>

      <div className={css.items}>
        <div className={css.col}>
          <div className={css.item}>
            <div
              className={css.bg}
              style={{
                backgroundImage: `url(${bg})`,
              }}
            />
            <div className={css.content}>
              <h3>Лизинг оборудования</h3>
              <p>
                Лизинг оборудования позволяет компании в короткий срок увеличить
                эффективность производства, снизить издержки, тем самым повысить
                свою конкурентоспособность.
              </p>
            </div>
          </div>
        </div>

        <div className={css.col}>
          <div className={css.item}>
            <div
              className={css.bg}
              style={{
                backgroundImage: `url(${bg})`,
              }}
            />
            <div className={css.content}>
              <h3>Лизинг спецтехники</h3>
              <p>
                Лизинг спецтехники позволит организации оперативно обновить или
                пополнить материально-техническую базу. С учетом высокой
                стоимости такой техники, оформление ее в лизинг экономически
                выгодно.
              </p>
            </div>
          </div>
        </div>

        <div className={css.col}>
          <div className={css.item}>
            <div
              className={css.bg}
              style={{
                backgroundImage: `url(${bg})`,
              }}
            />
            <div className={css.content}>
              <h3>Автолизинг</h3>
              <p>
                Лизинг легкового и коммерческого автотранспорта — эффективный
                способ обновления или расширения автопарка
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
);

export default Listings;
