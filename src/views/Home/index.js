import React from 'react';
import Slider from 'react-slick';
import Listings from './Listings';
import Calc from './Calc';
import Company from './Company';
import Footer from './Footer';
import Services from './Services';
import Feedback from './Feedback';
import css from './styles.module.scss';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import bg from 'assets/bg-1.jpg';

const Home = () => {
  const settings = {
    dots: true,
    arrows: false,
    infinite: true,
    fade: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
  };

  return (
    <div>
      <div className={css.wrapper}>
        <div className={css.slider}>
          <div className="container">
            <Slider {...settings} className={css.customSlider}>
              <div className={css.item}>
                <div className={css.content}>
                  <h3>Оборудования в лизине</h3>
                  <p>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                    Doloribus sed perferendis aliquid omnis, recusandae,
                    possimus accusantium iusto expedita quo repudiandae
                    necessitatibus saepe adipisci harum illo facere ipsam odit
                    perspiciatis est!
                  </p>
                  <button type="button">
                    <span>Подробнее</span>
                  </button>
                </div>
                <div
                  className={css.bg}
                  style={{
                    backgroundImage: `url(${bg})`,
                  }}
                />
              </div>

              <div className={css.item}>
                <div className={css.content}>
                  <h3>Лизинг недвижимости</h3>
                  <p>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                    Doloribus sed perferendis aliquid omnis, recusandae,
                    possimus accusantium iusto expedita quo repudiandae
                    necessitatibus saepe adipisci harum illo facere ipsam odit
                    perspiciatis est!
                  </p>
                  <button type="button">
                    <span>Подробнее</span>
                  </button>
                </div>
                <div
                  className={css.bg}
                  style={{
                    backgroundImage: `url(${bg})`,
                  }}
                />
              </div>
            </Slider>
          </div>
        </div>

        <Services />
        <Listings />
        <Calc />
        <Company />
        <Feedback />
        <Footer />
      </div>
    </div>
  );
};

export default Home;
