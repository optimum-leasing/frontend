import React from 'react';
import css from './styles.module.scss';


const Footer = () => (
  <footer className={css.footer}>
    <div className="container">
      <div className={css.items}>
        <div className={css.col}>
          <ul>
            <li>Лизинг</li>
            <li><a href="#">Пункт 1</a></li>
            <li><a href="#">Пункт 2</a></li>
            <li><a href="#">Пункт 3</a></li>
            <li><a href="#">Пункт 4</a></li>
          </ul>
        </div>

        <div className={css.col}>
          <ul>
            <li>Компания</li>
            <li><a href="#">Пункт 1</a></li>
            <li><a href="#">Пункт 2</a></li>
            <li><a href="#">Пункт 3</a></li>
            <li><a href="#">Пункт 4</a></li>
          </ul>
        </div>

        <div className={css.col}>
          {/* <ul>
            <li>Компания</li>
            <li><a href="#">Пункт 1</a></li>
            <li><a href="#">Пункт 2</a></li>
            <li><a href="#">Пункт 3</a></li>
            <li><a href="#">Пункт 4</a></li>
          </ul> */}
        </div>

        <div className={css.col}>
          <ul>
            <li>Связаться</li>
            <li><a href="#">Контакты</a></li>
          </ul>

          <div className={css.contacts}>
            <a href="#"> 8 (999) 999-99-99</a>
            <a href="#"> test@mail.ru</a>
            <a href="#"> 344010 г. Ростов-на-дону ул, Космонатов</a>
          </div>
        </div>


      </div>

      <div className={css.coop}>
        Lorem ipsum dolor sit amet consectetur, adipisicing elit. Harum vero quas, quasi sunt autem natus iusto! Iste dolore eum quae? Placeat suscipit repellat, omnis cupiditate sunt adipisci praesentium earum voluptatem.
      </div>
    </div>
  </footer>
);

export default Footer;
