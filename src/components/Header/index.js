import React, { useEffect, useState } from 'react';
import cn from 'classnames';
import { Link } from 'react-router-dom';
import logo from 'assets/logo.png';
import css from './styles.module.scss';

const Header = () => {
  const [fixed, setFixed] = React.useState(false);

  useEffect(() => {
    document.addEventListener('scroll', onScroll);

    return () => document.removeEventListener('scroll', onScroll);
  }, []);

  const onScroll = (event) => {
    if (event.srcElement.scrollingElement.scrollTop < 125) {
      return setFixed(false);
    }

    return setFixed(true);
  };

  return (
    <header className={cn(css.header, { _moved: fixed })}>
      <div className="container">
        <div className={css.top}>
          <div className="row">
            <Link to="/" className={css.logo}>
              <img src={logo} alt="" />
            </Link>
            <div className={css.feedback}>
              <a href="tel:899999999">+7 (999) 999-99-99</a>
              <button type="button"> Оставить заявку</button>
            </div>
          </div>
        </div>
        <div className={css.bottom}>
          <div className="row">
            <div className={css.nav}>
              <Link to="/">О компании</Link>
              <Link to="/">Лизинг</Link>
              <Link to="/">Калькулятор</Link>
              <Link to="/">Контакты</Link>
            </div>
          </div>
        </div>
      </div>
    </header>
  );
};

export default Header;
